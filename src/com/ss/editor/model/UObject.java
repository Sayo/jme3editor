package com.ss.editor.model;

/**
 * Интерфейс для реализации уникального объекта.
 *
 * @author Ronn
 */
public interface UObject {

    /**
     * @return уникальный ид объекта.
     */
    public long getObjectId();
}
