package com.ss.editor.ui;

import com.ss.editor.manager.FileIconManager;

import javafx.scene.image.Image;

/**
 * The interface with all icons of this application.
 *
 * @author JavaSaBr
 */
public interface Icons {

    FileIconManager ICON_MANAGER = FileIconManager.getInstance();

    Image SAVE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/save.png", 16);
    Image SCALE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/scale.png", 16);
    Image ROTATION_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/rotation.png", 16);
    Image CUBE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/cube.png");
    Image MOVE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/move.png", 16);
    Image LIGHT_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/light.png", 16);
    Image INFLUENCER_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/influencer.png");
    Image SPHERE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/sphere.png");
    Image PLANE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/plane.png");
    Image NODE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/node.png");
    Image PARTICLES_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/particles.png");
    Image PARTICLE_GEOMETRY_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/particle_geometry.png");
    Image GEOMETRY_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/geometry.png");
    Image MESH_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/mesh.png");
    Image EDIT_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/edit.png");
    Image AMBIENT_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/ambient.png");
    Image LAMP_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/lamp.png");
    Image POINT_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/point.png");
    Image SUN_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/sun.png");
    Image PLAY_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/play.png");
    Image STOP_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/stop.png");
    Image ANIMATION_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/animation.png");
    Image ANI_CHANNEL_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/anim_channel.png");
    Image GEAR_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/gear.png");
    Image BONE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/bone.png");
    Image AUDIO_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/audio.png");
    Image SETTINGS_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/settings.png");
    Image PASTE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/paste.png");
    Image VIEW_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/view.png");
    Image NEW_FILE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/new_file.png");
    Image CUT_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/cut.png");
    Image COPY_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/copy.png");
    Image TRANSFORMATION_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/transformation.png");
    Image EXTRACT_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/extract.png");
    Image GENERATE_16 = ICON_MANAGER.getImage("/ui/icons/actions/16/generate_16.png");

    Image REFRESH_18 = ICON_MANAGER.getImage("/ui/icons/actions/18/refresh.png", 18);
    Image REMOVE_18 = ICON_MANAGER.getImage("/ui/icons/actions/18/remove.png", 18);
    Image ADD_18 = ICON_MANAGER.getImage("/ui/icons/actions/18/add.png", 18);
    Image CLOSE_18 = ICON_MANAGER.getImage("/ui/icons/actions/18/close.png", 18);

    Image SAVE_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/save.png", 24);
    Image ADD_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/add.png", 24);
    Image LIGHT_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/light.png", 24);
    Image ROTATION_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/rotation.png", 24);
    Image MOVE_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/move.png", 24);
    Image SCALE_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/scale.png", 24);
    Image CUBE_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/cube.png", 24);
    Image SPHERE_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/sphere.png", 24);
    Image PLANE_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/plane.png", 24);
    Image ADD_CIRCLE_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/add_circle.png", 24);
    Image IMAGE_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/image.png", 24);
    Image WARNING_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/warning.png", 24);
    Image EDIT_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/edit.png", 24);
    Image FROM_FULLSCREEN_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/from_fullscreen.png", 24);
    Image TO_FULLSCREEN_24 = ICON_MANAGER.getImage("/ui/icons/actions/24/to_fullscreen.png", 24);

    Image PLAY_128 = ICON_MANAGER.getImage("/ui/icons/actions/128/play.png", 128);
    Image PAUSE_128 = ICON_MANAGER.getImage("/ui/icons/actions/128/pause.png", 128);
    Image STOP_128 = ICON_MANAGER.getImage("/ui/icons/actions/128/stop.png", 128);

    Image IMAGE_512 = ICON_MANAGER.getImage("/ui/icons/512/image.png", 512);
}
